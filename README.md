# OpenML dataset: svmguide3

https://www.openml.org/d/1589

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Chih-Wei Hsu","Chih-Chung Chang","and Chih-Jen Lin.  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**:   

#Dataset from the LIBSVM data repository.

Preprocessing: Original data: someone from Germany working with the car industry.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1589) of an [OpenML dataset](https://www.openml.org/d/1589). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1589/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1589/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1589/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

